#!/usr/bin/env python3

import sys
import secrets

def generatorPassword(lenght=8):
	if (lenght <= 0):
		raise Exception('The lenght of the password must be greater than zero.')

	return secrets.token_urlsafe(lenght)


password = ''

if (len(sys.argv) == 3):
	lenght = int(sys.argsv[2])
	password = generatorPassword(lenght)

else:
	password = generatorPassword()

print(password)
